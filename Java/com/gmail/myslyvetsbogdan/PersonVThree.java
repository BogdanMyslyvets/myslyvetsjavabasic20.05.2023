package com.gmail.myslyvetsbogdan;

public class PersonVThree {
    private String name;
    private String surname;
    private String city;
    private String phone;

    public PersonVThree(String name, String surname, String city, String phone) {
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.phone = phone;
    }

    public PersonVThree() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String personInfo() {
        String info = "You can call a citizen " + name + " " + surname + " from city " + city + " by number " + phone;
        return info;
    }


    public static void main(String[] args) {
        int count = 3;
        PersonVThree[] person = new PersonVThree[count];

        person[0] = new PersonVThree("Will", "Smith", "New York", "2936729462846");
        person[1] = new PersonVThree("Jackie", "Chan", "Shanghai", "12312412412");

        person[2] = new PersonVThree();
        person[2].setName("Sherlock");
        person[2].setSurname("Holmes");
        person[2].setCity("London");
        person[2].setPhone("37742123513");

        print(person);
    }

    public static void print(PersonVThree[] person) {
        char character = 'a';
        for (int i = 0; i < person.length; i++) {
            System.out.println(character + ". " + person[i].personInfo());
            character++;
        }
    }
}
