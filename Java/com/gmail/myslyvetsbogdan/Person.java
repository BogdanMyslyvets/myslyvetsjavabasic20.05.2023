package com.gmail.myslyvetsbogdan;

public class Person {
    public String name;
    public String surname;
    public String city;
    public String phone;

    public static String personInfo(String name, String surname, String city, String phone) {
        String info = "You can call a citizen " + name + " " + surname + " from city " + city + " by number " + phone;
        return info;
    }

    public static void main(String[] args) {
        Person onePerson = new Person();
        onePerson.name = "Will";
        onePerson.surname = "Smith";
        onePerson.city = "New York";
        onePerson.phone = "2936729462846";

        Person twoPerson = new Person();
        twoPerson.name = "Jackie";
        twoPerson.surname = "Chan";
        twoPerson.city = "Shanghai";
        twoPerson.phone = "12312412412";

        Person threePerson = new Person();
        threePerson.name = "Sherlock";
        threePerson.surname = "Holmes";
        threePerson.city = "London";
        threePerson.phone = "37742123513";

        System.out.print("a.");
        System.out.println(personInfo(onePerson.name,onePerson.surname,onePerson.city,onePerson.phone));
        System.out.print("b.");
        System.out.println(personInfo(twoPerson.name,twoPerson.surname,twoPerson.city,twoPerson.phone));
        System.out.print("c.");
        System.out.println(personInfo(threePerson.name,threePerson.surname,threePerson.city,threePerson.phone));
    }
}
