package com.gmail.myslyvetsbogdan;

import java.util.Random;
import java.util.Scanner;

public class MultidimensionalArray {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Enter the number of rows:");
        int max = 10;
        int rovs = input.nextInt();      // N
        System.out.println("Enter the maximum number of items in a row:");
        int maxAmount = input.nextInt(); // M max
        int[][] randomNumbers = new int[rovs][];
        addAmountElements(randomNumbers, maxAmount, random);
        fillingArray(randomNumbers, max, random);
        System.out.println("The original array:");
        printTwoDimensionalArray(randomNumbers);
        sortsElements(randomNumbers);
        System.out.println("Sorted array:");
        printTwoDimensionalArray(randomNumbers);
        long sum = countSum(randomNumbers);
        System.out.println("The sum of all elements of a two-dimensional stepped array: " + sum);
        int[] minimalElements = new int[rovs];
        fillingMinimalElements(minimalElements, randomNumbers);
        System.out.println("Minimum elements for each row:");
        printOneDimensionalArray(minimalElements);
        int absoluteMinimum = searchAbsoluteMinimum(minimalElements);
        System.out.println("Minimum element among all elements (absolute minimum): " + absoluteMinimum);
        divide(absoluteMinimum, sum);

    }

    private static void divide(int absoluteMinimum, long sum) {
        if (absoluteMinimum == 0) {
            System.out.println("The absolute minimum: 0, cannot be divided by 0");
        } else {
            System.out.println("The result of dividing all " +
                    "the elements of the stepped array by the absolute minimum: " + sum / absoluteMinimum);
        }
    }

    private static int searchAbsoluteMinimum(int[] array) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }

    private static void printOneDimensionalArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i < array.length - 1) {
                System.out.print(array[i] + ", ");
            } else {
                System.out.print(array[i] + ".");
            }
        }
        System.out.println();
    }

    private static void fillingMinimalElements(int[] array, int[][] randomNumbers) {
        for (int i = 0; i < array.length; i++) {
            array[i] = searchMinimumValue(randomNumbers, i);
        }
    }

    private static int searchMinimumValue(int[][] array, int index) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array[index].length; i++) {
            if (array[index][i] < min) {
                min = array[index][i];
            }
        }
        return min;
    }

    private static int countSum(int[][] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum = sum + array[i][j];
            }
        }
        return sum;
    }

    private static void sortsDescending(int[][] array, int index) {
        int tmp;
        for (int j = 0; j < array[index].length - 1; j++) {
            for (int i = 0; i < array[index].length - j - 1; i++) {
                if (array[index][i] < array[index][i + 1]) {
                    tmp = array[index][i];
                    array[index][i] = array[index][i + 1];
                    array[index][i + 1] = tmp;
                }
            }
        }
    }

    private static void sortsAscending(int[][] array, int index) {
        int tmp;
        for (int j = 0; j < array[index].length - 1; j++) {
            for (int i = 0; i < array[index].length - j - 1; i++) {
                if (array[index][i] > array[index][i + 1]) {
                    tmp = array[index][i];
                    array[index][i] = array[index][i + 1];
                    array[index][i + 1] = tmp;
                }
            }
        }
    }

    private static void sortsElements(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                sortsAscending(array, i);
            } else {
                sortsDescending(array, i);
            }
        }
    }

    private static void printTwoDimensionalArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (j < array[i].length - 1) {
                    System.out.print(array[i][j] + ", ");
                } else {
                    System.out.print(array[i][j] + ".");
                }
            }
            System.out.println();
        }
    }

    private static void fillingArray(int[][] array, int max, Random random) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = givesRandomNumber(max, random);
            }
        }
    }

    private static void addAmountElements(int[][] array, int max, Random random) {
        for (int i = 0; i < array.length; i++) {
            array[i] = new int[givesRandomNumberRange(max, random)];
        }
    }

    private static int givesRandomNumberRange(int max, Random random) {
        return random.nextInt(max - 1) + 1;
    }

    private static int givesRandomNumber(int max, Random random) {
        return random.nextInt(max);
    }
}
