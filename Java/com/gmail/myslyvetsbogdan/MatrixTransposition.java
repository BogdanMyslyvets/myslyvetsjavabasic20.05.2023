package com.gmail.myslyvetsbogdan;

import java.util.Scanner;

public class MatrixTransposition {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the height of the matrix");
        int m = input.nextInt();
        System.out.println("Enter the width of the matrix");
        int n = input.nextInt();  //width matrix
        int[][] originalMatrix = new int[m][n];
        int[][] transposedMatrix = new int[n][m];
        originalMatrixFilling(originalMatrix);
        System.out.println("Original matrix:");
        printMatrix(originalMatrix);
        transpositionMatrix(originalMatrix, transposedMatrix);
        System.out.println("Transposed matrix:");
        printMatrix(transposedMatrix);
    }

    public static void transpositionMatrix(int[][] matrix, int[][] newMatrix) {
        for (int i = 0; i < newMatrix.length; i++) {
            for (int j = 0; j < newMatrix[i].length; j++) {
                newMatrix[i][j] = matrix[j][i];
            }
        }

    }

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void originalMatrixFilling(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (i + 1) * (j + 1);
            }
        }
    }
}
