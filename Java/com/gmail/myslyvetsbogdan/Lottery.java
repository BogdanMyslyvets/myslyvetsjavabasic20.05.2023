package com.gmail.myslyvetsbogdan;

import java.util.Random;

public class Lottery {
    public static void main(String[] args) {
        Random random = new Random();
        int amount = 7;
        int minNumber = 0;
        int maxNumber = 9;
        int[] numbersIndicated = new int[amount];
        int[] numbersGuessed = new int[amount];
        addRandomNumber(numbersIndicated, minNumber, maxNumber, random);
        addRandomNumber(numbersGuessed, minNumber, maxNumber, random);
        // printArray(numbersIndicated);
        // printArray(numbersGuessed);
        // System.out.println("______");
        sortArray(numbersIndicated);
        sortArray(numbersGuessed);
        printArray(numbersIndicated);
        printArray(numbersGuessed);
        System.out.println("Number of matches: " + checkMatches(numbersIndicated, numbersGuessed));
    }

    private static int checkMatches(int[] indicated, int[] guessed) {
        int count = 0;
        for (int i = 0; i < indicated.length; i++) {
            if (indicated[i] == guessed[i]) {
                count++;
            }
        }
        return count;
    }

    private static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (i < array.length - 1) {
                System.out.print(array[i] + ", ");
            } else {
                System.out.print(array[i] + ".");
            }
        }
        System.out.println();
    }

    private static void sortArray(int[] array) {
        int tmp;
        for (int j = 0; j < array.length - 1; j++) {
            for (int i = 0; i < array.length - j - 1; i++) {
                if (array[i] > array[i + 1]) {
                    tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                }
            }
        }
    }

    public static void addRandomNumber(int array[], int min, int max, Random random) {
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(max - min + 1) + min;
        }
    }
}
