package com.gmail.myslyvetsbogdan;

public class PersonVTwo {
    public String name;
    public String surname;
    public String city;
    public String phone;

    public static String personInfo(PersonVTwo person) {
        String info = "You can call a citizen " + person.name + " " + person.surname + " from city " + person.city + " by number " + person.phone;
        return info;
    }

    public static void main(String[] args) {
        int count = 5;
        PersonVTwo[] countPerson = new PersonVTwo[count];

        countPerson[0] = new PersonVTwo();
        countPerson[0].name = "Will";
        countPerson[0].surname = "Smith";
        countPerson[0].city = "New York";
        countPerson[0].phone = "2936729462846";

        countPerson[1] = new PersonVTwo();
        countPerson[1].name = "Jackie";
        countPerson[1].surname = "Chan";
        countPerson[1].city = "Shanghai";
        countPerson[1].phone = "12312412412";

        countPerson[2] = new PersonVTwo();
        countPerson[2].name = "Sherlock";
        countPerson[2].surname = "Holmes";
        countPerson[2].city = "London";
        countPerson[2].phone = "37742123513";

        System.out.print("a.");
        System.out.println(personInfo(countPerson[0]));
        System.out.print("b.");
        System.out.println(personInfo(countPerson[1]));
        System.out.print("c.");
        System.out.println(personInfo(countPerson[2]));
    }
}
