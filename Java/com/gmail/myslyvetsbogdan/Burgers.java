package com.gmail.myslyvetsbogdan;

public class Burgers {
    private String bun;
    private String meat;
    private String cheese;
    private String greens;
    private String mayonnaise;


    public Burgers(String bun, String meat, String cheese, String greens, String mayonnaise) {
        this.bun = bun;
        this.meat = meat;
        this.cheese = cheese;
        this.greens = greens;
        this.mayonnaise = mayonnaise;

        System.out.println("The composition of a regular burger: " + bun + ", " + meat + ", " + cheese + ", " + greens + ", " + mayonnaise + ".");
    }

    public Burgers(String bun, String meat, String cheese, String greens) {
        this.bun = bun;
        this.meat = meat;
        this.cheese = cheese;
        this.greens = greens;

        System.out.println("Diet burger composition: " + bun + ", " + meat + ", " + cheese + ", " + greens + ".");
    }

    public Burgers(String bun, String amountMeat, String meat, String cheese, String greens, String mayonnaise) {
        this.bun = bun;
        this.meat = amountMeat + " " + meat;
        this.cheese = cheese;
        this.greens = greens;
        this.mayonnaise = mayonnaise;

        System.out.println("The composition of the burger with double meat: " +
                bun + ", " + amountMeat + " " + meat + ", " + cheese + ", " + greens + ", " + mayonnaise + ".");
    }

}

class BurgerMain {

    public static void main(String[] args) {
        Burgers regularBurgers = new Burgers("bun", "meat", "cheese", "greens", "mayonnaise");
        Burgers dietBurgers = new Burgers("bun", "meat", "cheese", "greens");
        Burgers doubleMeatBurgers = new Burgers("bun", "double", "meat", "cheese", "greens", "mayonnaise");
    }
}

