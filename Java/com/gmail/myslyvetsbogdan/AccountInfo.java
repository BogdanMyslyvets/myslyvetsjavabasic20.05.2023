package com.gmail.myslyvetsbogdan;

public class AccountInfo {
    private static short year = 2020;
    private byte age;
    private String name;
    private String dayOfBirth;
    private String monthOfBirth;
    private short yearOfBirth;
    private String email;
    private String phone;

    private String surname;
    private short weight;
    private String pressure;
    private int countSteps;

    public AccountInfo(String name, String dayOfBirth, String monthOfBirth, short yearOfBirth, String email, String phone, String surname, short weight, String pressure, int countSteps) {
        this.name = name;
        this.dayOfBirth = dayOfBirth;
        this.monthOfBirth = monthOfBirth;
        this.yearOfBirth = yearOfBirth;
        this.age = (byte) (year - yearOfBirth);
        this.email = email;
        this.phone = phone;
        this.surname = surname;
        this.weight = weight;
        this.pressure = pressure;
        this.countSteps = countSteps;
    }

    public short getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getDayOfBirth() {
        return dayOfBirth;
    }

    public String getMonthOfBirth() {
        return monthOfBirth;
    }

    public short getYearOfBirth() {
        return yearOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getSurname() {
        return surname;
    }

    public short getWeight() {
        return weight;
    }

    public String getPressure() {
        return pressure;
    }

    public int getCountSteps() {
        return countSteps;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setWeight(short weight) {
        this.weight = weight;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public void setCountSteps(int countSteps) {
        this.countSteps = countSteps;
    }

    public static void main(String[] args) {
        int count = 3;
        AccountInfo[] account = new AccountInfo[count];

        account[0] = new AccountInfo("Roman", "01", "01", (short) 2000, "1@gmail.com",
                "073-000-00-01", "Romanovskiy", (short) 70, "120/60", 1000);
        account[1] = new AccountInfo("Stepan", "02", "02", (short) 2001, "2@gmail.com",
                "073-000-00-02", "Stepanovskiy", (short) 80, "120/70", 2000);
        account[2] = new AccountInfo("Ivan", "03", "03", (short) 2002, "3@gmail.com",
                "073-000-00-03", "Ivanovskiy", (short) 90, "120/80", 3000);

        for (AccountInfo num : account) {
            num.printAccountInfo();
        }

        System.out.println("--------------------------------");
        System.out.println("--------------------------------");
        System.out.println("--------------------------------");

        account[0].setCountSteps(5000);
        account[0].setPressure("130/70");

        account[2].setCountSteps(10000);
        account[2].setWeight((short) 60);

        for (AccountInfo num : account) {
            num.printAccountInfo();
        }

    }

    public void printAccountInfo() {
        System.out.println("Information about the user: ");
        System.out.println("Name " + getName() + "," + " surname " + getSurname());
        System.out.println("Birthday " + getDayOfBirth() + "." + getMonthOfBirth() + "." + getYearOfBirth());
        System.out.println("Email " + getEmail());
        System.out.println("Phone " + getPhone());
        System.out.println("Wight " + getWeight());
        System.out.println("Pressure " + getPressure());
        System.out.println("Count steps " + getCountSteps());
        System.out.println(getName() + " is " + getAge() + " years old");
        System.out.println("________________________________");
    }

}

