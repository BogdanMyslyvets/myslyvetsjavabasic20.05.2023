package com.gmail.myslyvetsdz21;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countError = 0;

        while (countError < 3) {
            try {
                System.out.print("Enter login: ");
                String login = scanner.nextLine();

                System.out.print("Enter password: ");
                String password = scanner.nextLine();

                System.out.print("Confirm password: ");
                String confirmPassword = scanner.nextLine();

                User user = new User(login, password, confirmPassword);

                System.out.println("User added.");
                break;
            } catch (WrongLoginException | WrongPasswordException e) {
                countError++;
                System.out.println("Error: " + e.getMessage());

                if (countError == 3) {
                    System.out.println("The number of attempts has ended, the program has ended.");
                    break;
                }
            }
        }

        System.out.println("Thank you for using our service.");
    }
}
