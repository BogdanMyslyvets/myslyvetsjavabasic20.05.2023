package com.gmail.myslyvetsdz21;

class WrongPasswordException extends Exception {
    public WrongPasswordException(String message) {
        super(message);
    }
}
