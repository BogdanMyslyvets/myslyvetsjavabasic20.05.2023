package com.gmail.myslyvetsdz21;

class WrongLoginException extends Exception {
    public WrongLoginException(String message) {
        super(message);
    }
}
