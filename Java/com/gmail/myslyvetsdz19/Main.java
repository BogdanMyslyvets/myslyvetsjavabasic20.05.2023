package com.gmail.myslyvetsdz19;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        while (true) {
            System.out.println("Drinks menu:");
            System.out.println("1 - Coffee");
            System.out.println("2 - Tea");
            System.out.println("3 - Lemonade");
            System.out.println("4 - Mojito");
            System.out.println("5 - Mineral water");
            System.out.println("6 - Coca Cola");
            System.out.println("0 - Exit");
            System.out.print("Enter your choice: ");

            Scanner scanner = new Scanner(System.in);
            int type = scanner.nextInt();
            scanner.nextLine();
            if (type == 0) {
                break;
            }

            String drinkType = getDrinkType(type);
            Drinks.doDrink(drinkType);
        }

        System.out.println("Total drinks count: " + Drinks.getOnlyDrinksCount());
        System.out.println("Total cost: " + Drinks.getOnlyCost() + " UAH");
    }

    private static String getDrinkType(int type) {
        return switch (type) {
            case 1 -> "COFFEE";
            case 2 -> "TEA";
            case 3 -> "LEMONADE";
            case 4 -> "MOJITO";
            case 5 -> "MINERAL_WATER";
            case 6 -> "COCA_COLA";
            default -> "";
        };
    }
}

