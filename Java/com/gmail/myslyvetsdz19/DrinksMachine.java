package com.gmail.myslyvetsdz19;

enum DrinksMachine {
    COFFEE,
    TEA,
    LEMONADE,
    MOJITO,
    MINERAL_WATER,
    COCA_COLA
}

class Drinks {
    private static final double COFFEE_COST = 40.00;
    private static final double TEA_COST = 25.00;
    private static final double LEMONADE_COST = 60.00;
    private static final double MOJITO_COST = 80.00;
    private static final double MINERAL_WATER_COST = 15.00;
    private static final double COCA_COLA_COST = 45.00;

    private static int onlyDrinksCount = 0;
    private static double onlyCost = 0.0;

    public static int getOnlyDrinksCount() {
        return onlyDrinksCount;
    }

    public static double getOnlyCost() {
        return onlyCost;
    }

    public static void doDrink(String drinkType) {
        try {
            DrinksMachine drink = DrinksMachine.valueOf(drinkType.toUpperCase());
            switch (drink) {
                case COFFEE -> {
                    System.out.println("Making coffee");
                    addCountCost(COFFEE_COST);
                }
                case TEA -> {
                    System.out.println("Making tea");
                    addCountCost(TEA_COST);
                }
                case LEMONADE -> {
                    System.out.println("Pouring lemonade");
                    addCountCost(LEMONADE_COST);
                }
                case MOJITO -> {
                    System.out.println("Making mojito");
                    addCountCost(MOJITO_COST);
                }
                case MINERAL_WATER -> {
                    System.out.println("Pouring mineral water");
                    addCountCost(MINERAL_WATER_COST);
                }
                case COCA_COLA -> {
                    System.out.println("Pouring Coca Cola");
                    addCountCost(COCA_COLA_COST);
                }
                default -> System.out.println("Invalid drink choice. Please try again.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println("There is no such drink, choose another.");
        }
    }

    private static void addCountCost(double price) {
        onlyDrinksCount++;
        onlyCost += price;
    }


}
