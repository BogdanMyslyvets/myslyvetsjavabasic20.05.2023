package com.gmail.myslyvetsbv;

public class Androids implements Smartphones, LinuxOS {
    private String name;

    public Androids(String name) {
        this.name = name;
    }

    @Override
    public void call() {
        System.out.println(name + " calling");
    }

    @Override
    public void sms() {
        System.out.println(name + " sending an SMS");
    }

    @Override
    public void internet() {
        System.out.println(name + " access the Internet");
    }

}
