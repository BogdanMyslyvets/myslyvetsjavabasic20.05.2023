package com.gmail.myslyvetsbv;

public class Phones {
    public static void main(String[] args) {
        Smartphones[] phones = new Smartphones[2];

        Androids samsung = new Androids("Samsung");
        IPhones iPhone = new IPhones("iPhone");

        phones[0] = samsung;
        phones[1] = iPhone;

        testPhone(phones);
    }

    private static void testPhone(Smartphones[] phones) {
        for (Smartphones phone : phones) {
            phone.call();
            phone.sms();
            phone.internet();
        }
    }
}

