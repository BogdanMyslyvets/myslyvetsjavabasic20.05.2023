package com.gmail.myslyvetsbv;

public class IPhones implements Smartphones, IOS {

    private String name;

    public IPhones(String name) {
        this.name = name;

    }

    @Override
    public void call() {
        System.out.println(name + " calling");
    }

    @Override
    public void sms() {
        System.out.println(name + " sending an SMS");
    }

    @Override
    public void internet() {
        System.out.println(name + " access the Internet");
    }
}
