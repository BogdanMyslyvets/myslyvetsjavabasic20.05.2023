package com.gmail.myslyvetsdz17;

public class Matrix {
    public static double calculateArithmeticMean(int[][] matrix) {
        int sum = 0;
        int count = 0;
        for (int j = 0; j < matrix.length; j++) {
            for (int i = 0; i < matrix[j].length; i++) {
                sum += matrix[j][i];
                count++;
            }
        }
        return (double) sum / count;
    }

    public static boolean checkSquarenessMatrix(int[][] matrix) {
        return matrix.length == matrix[0].length;
    }


}


