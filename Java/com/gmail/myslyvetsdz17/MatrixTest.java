package com.gmail.myslyvetsdz17;

public class MatrixTest {
    public void calculateArithmeticMean() {
        // given
        int[][] matrix = {{1, 1, 1}, {3, 3, 3}};
        double expected = 2;

        //when
        double result = Matrix.calculateArithmeticMean(matrix);

        //then проверка
        System.out.println(result == expected);
    }

    public void checkSquarenessMatrix() {
        // given
        int[][] matrix = new int[3][3];
        boolean expected = true;

        //when
        boolean result = Matrix.checkSquarenessMatrix(matrix);

        //then проверка
        System.out.println(result == expected);
    }

    public void checkNotSquarenessMatrix() {
        // given
        int[][] matrix = new int[3][2];
        boolean expected = false;

        //when
        boolean result = Matrix.checkSquarenessMatrix(matrix);

        //then проверка
        System.out.println(result == expected);
    }

    public static void main(String[] args) {
        MatrixTest test = new MatrixTest();
        test.calculateArithmeticMean();
        test.checkSquarenessMatrix();
        test.checkNotSquarenessMatrix();

        int[][] m = {{1, 1, 1}, {3, 3, 3}};
        System.out.println(Matrix.calculateArithmeticMean(m));


    }
}
