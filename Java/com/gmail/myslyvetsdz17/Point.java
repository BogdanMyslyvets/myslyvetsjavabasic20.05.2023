package com.gmail.myslyvetsdz17;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Point implements Cloneable {
    private static final int x = 0;
    private static final int y = 0;
    private int xPoint;
    private int yPoint;
    private String name;


    public Point(int xPoint, int yPoint, String name) {
        this.xPoint = xPoint;
        this.yPoint = yPoint;
        this.name = name;
    }

    public Point(Point point) {
        this.xPoint = point.xPoint;
        this.yPoint = point.yPoint;
        this.name = point.name;
    }

    public int getxPoint() {
        return xPoint;
    }

    public int getyPoint() {
        return yPoint;
    }

    public String getName() {
        return name;
    }

    public void setxPoint(int xPoint) {
        this.xPoint = xPoint;
    }

    public void setyPoint(int yPoint) {
        this.yPoint = yPoint;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "point " + getName() + " {" +
                "x=" + xPoint +
                ", y=" + yPoint +
                '}';
    }

    @Override
    protected Point clone() throws CloneNotSupportedException {
        return (Point) super.clone();
    }

    public static void printLine() {
        System.out.println("______________________________________");
    }


    public static void pointComparison(Point one, Point two) {
        if (one.getxPoint() == two.getxPoint() && one.getyPoint() == two.getyPoint()) {
            System.out.println("Points " + one.getName() + " and " + two.getName() + " are equal");
        } else {
            System.out.println("Dots " + one.getName() + " and " + two.getName() + " are not equal");
        }
    }

    public static double distanceCalculationAB(Point one, Point two) {
        return sqrt(pow((one.getxPoint() - two.getxPoint()), 2) + pow((one.getyPoint() - two.getyPoint()), 2));
    }

    public static double distanceCalculation(Point point) {
        return sqrt(pow((x - point.getxPoint()), 2) + pow((y - point.getyPoint()), 2));
    }

    public static void printInfo(Point point) {
        System.out.println("Coordinates " + point);
    }

    public static void printDistance(Point point) {
        System.out.println("Distance to point " + point.getName() + " = " + String.format("%.2f", distanceCalculation(point)));
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Point a = new Point(7, 7, "A");
        Point b = new Point(8, 8, "B");
        printInfo(a);
        printInfo(b);
        printLine();

        Point c = new Point(a);
        c.setName("C");
        System.out.println("Clone C from A:");
        printInfo(c);
        printLine();

        Point d = b.clone();
        d.setName("D");
        System.out.println("Clone D from B:");
        printInfo(d);
        printLine();

        pointComparison(a, b);
        printLine();

        System.out.println("Changed coordinates at point A and B");
        a.setxPoint(5);
        a.setyPoint(5);
        b.setxPoint(5);
        b.setyPoint(5);
        printInfo(a);
        printInfo(b);
        printLine();

        System.out.println("Check if point cloning has changed");
        printInfo(c);
        printInfo(d);
        printLine();

        pointComparison(a, b);
        printLine();

        System.out.println("Distance between points A and D = :" + String.format("%.2f", distanceCalculationAB(a, d)));
        printLine();


        printDistance(a);
        printDistance(b);
        printDistance(c);
        printDistance(d);
        printLine();


    }
}
