package com.gmail.myslyvetsdz20;

public class Main {
    public static void main(String[] args) {
        MusicStyles[] stylesGroup = new MusicStyles[3];
        stylesGroup[0] = new ClassicMusic();
        stylesGroup[1] = new RockMusic();
        stylesGroup[2] = new PopMusic();

        for (MusicStyles styles : stylesGroup){
            styles.playMusic();
        }
    }
}
